use actix_web::{self, server, HttpRequest, Responder};
use clap::{self, Arg};
use redis::{self, Commands};
use std::collections::HashMap;

struct AppState {
    redis_url: String,
}

fn get_all(req: &HttpRequest<AppState>) -> impl Responder {
    let client = redis::Client::open(&*req.state().redis_url).unwrap();
    let con = client.get_connection().unwrap();
    let val: HashMap<String, i32> = con.hgetall("score").unwrap();
    format!("{:#?}", val)
}

fn get(req: &HttpRequest<AppState>) -> impl Responder {
    let name = req.match_info().get("name").unwrap();
    let client = redis::Client::open(&*req.state().redis_url).unwrap();
    let con = client.get_connection().unwrap();
    let val: isize = con.hget("score", name).unwrap();
    format!("{}", val)
}

fn set(req: &HttpRequest<AppState>) -> impl Responder {
    let name = req.match_info().get("name").unwrap();
    let score = req
        .match_info()
        .get("score")
        .unwrap()
        .parse::<usize>()
        .unwrap();;
    let client = redis::Client::open(&*req.state().redis_url).unwrap();
    let con = client.get_connection().unwrap();
    let val: isize = con.hset("score", name, score).unwrap();
    format!("{}", val)
}

fn main() {
    let default_port = "8000";
    let default_redis_url = "redis://127.0.0.1/";

    let matches = clap::App::new("My Super Scoreboard")
        .version("1.0")
        .author("Thierry Berger. <contact@thierryberger.com>")
        .about("Simplest scoreboard with redis")
        .arg(
            Arg::with_name("port")
                .short("p")
                .multiple(false)
                .takes_value(true)
                .help(&format!(
                    "Sets the port the server listens to. Default set to {}",
                    default_port
                )),
        )
        .arg(
            Arg::with_name("redisurl")
                .short("r")
                .multiple(false)
                .takes_value(true)
                .help(&format!(
                    "Sets the url the server uses to connect to redis. Default set to {}",
                    default_redis_url
                )),
        )
        .get_matches();
    let port_to_listen_to = matches.value_of("port").unwrap_or(default_port);
    let redis_url = matches
        .value_of("redisurl")
        .unwrap_or(default_redis_url)
        .to_string();

    println!("Server will listen to port {}; and connect to redis server {}", port_to_listen_to, redis_url);
    server::new(move || {
        actix_web::App::with_state(AppState {
            redis_url: redis_url.clone(),
        })
        .resource("/get", |r| r.f(get_all))
        .resource("/get/{name}", |r| r.f(get))
        .resource("/set/{name}/{score}", |r| r.f(set))
    })
    .bind(format!("0.0.0.0:{}", port_to_listen_to))
    .expect(&format!("Can not bind to port {}", port_to_listen_to))
    .run();
}
